<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test Mail</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <h1>This is a test mail</h1>
        <p>Hi.</p>
        <p>This is a test mail.</p>
        <p><b>{{ $details }}</b></p>
    </body>
</html>
