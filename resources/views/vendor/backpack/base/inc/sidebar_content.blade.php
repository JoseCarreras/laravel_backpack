<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href="{{ backpack_url('log') }}"><i class='nav-icon fa fa-terminal'></i> Logs</a></li>
<li class='nav-item'><a class='nav-link' href="{{ backpack_url('card') }}"><i class='nav-icon fa fa-cube'></i> Cards</a></li>
<li class='nav-item'><a class='nav-link' href="{{ backpack_url('collection') }}"><i class='nav-icon fa fa-cubes'></i> Collections</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('worker') }}'><i class='nav-icon fa fa-users'></i> Workers</a></li>