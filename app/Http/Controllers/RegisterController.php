<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Helpers\Token;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $user = new User();
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->name = $request->name;
        try
        {
            $user->save();
            
        }
        catch (\Throwable $th)
        {
            if ($th->getCode() == "23000")
            {
                return response()->json([
                    'message' => 'user exist',
                ], 400);
            }
        }

        $details = [
            'title' => 'Mail from Vanadis',
            'body' => 'mail from: '.$user->email,
        ];

        Mail::to('jose.carreras@vanadis.es')->send(new TestMail($details));

        $data_token = ['email' => $user->email];
        $token_builder = new Token($data_token);
        $token = $token_builder->encode();

        return response()->json([
            'token' => $token,
            'email' => 'joe@doe.com',
            'name' => 'joe'
        ], 200);
    }
}
