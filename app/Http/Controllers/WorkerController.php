<?php

namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    public function get_all_workers(){

        $response=[];                                      

        $workers = Worker::all();
        
        if (!$workers->isEmpty()){              

            for ($i=0; $i <count($workers) ; $i++) {                  

                $response[$i] = [
                    "name" => $workers[$i]->name,
                    "nickname" => $workers[$i]->nickname,				               				
                    "job" => $workers[$i]->job,				               				
                    "secret" => $workers[$i]->secret,				               				
                ];
            }
        }else{            
            $response = "No workers in Vanadis";
        }           
        
        return response()->json($response);
    }
}
