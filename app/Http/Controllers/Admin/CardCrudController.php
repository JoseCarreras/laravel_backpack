<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CardRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

/**
 * Class CardCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CardCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

  

  public function fetchCollection()
  {
    return $this->fetch(\App\Models\Collection::class);
  }

  public function setup()
  {
    $this->crud->setModel('App\Models\Card');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/card');
    $this->crud->setEntityNameStrings('card', 'cards');
    $this->crud->enableExportButtons();
  }
  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    $this->crud->setFromDb();

    //Sets the image column as a small image display
    $this->crud->modifyColumn('image', [
      'name'      => 'image', // The db column name
      'label'     => 'Profile image', // Table column heading
      'type'      => 'image',
    ]);

    /* FILTERS */

    //Date range Filter
    $this->crud->addFilter(
      [
        'type'  => 'date_range',
        'name'  => 'from_to',
        'label' => 'Date range'
      ],
      false,
      function ($value) {
        $dates = json_decode($value);
        $this->crud->addClause('where', 'created_at', '>=', $dates->from);
        $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
      }
    );

    //Search by name Filter
    $this->crud->addFilter(
      [
        'type'  => 'text',
        'name'  => 'name',
        'label' => 'Name'
      ],
      false,
      function ($value) {
        $this->crud->addClause('where', 'name', 'LIKE', "%$value%");
      }
    );


    //Select category filter
    $this->crud->addFilter([
      'name'  => 'collection_id',
      'type'  => 'select2',
      'label' => 'Collection'
    ], function () {
      return \app\Models\Collection::all()->keyBy('id')->pluck('name', 'id')->toArray();
    }, function ($value) {
      $this->crud->addClause('where', 'collection_id', $value);
    });

    //Filters by quantity
    $this->crud->addFilter(
      [
        'name'       => 'quantity',
        'type'       => 'range',
        'label'      => 'Quantity range',
        'label_from' => 'min value',
        'label_to'   => 'max value'
      ],
      false,
      function ($value) {
        $range = json_decode($value);
        if ($range->from) {
          $this->crud->addClause('where', 'quantity', '>=', (int) $range->from);
        }
        if ($range->to) {
          $this->crud->addClause('where', 'quantity', '<=', (int) $range->to);
        }
      }
    );

    // Status filter
    $this->crud->addFilter([
      'name'  => 'status',
      'type'  => 'select2',
      'label' => 'Status'
    ], function () {
      return \App\Models\Card::getEnumValuesAsAssociativeArray('status');
    }, function ($value) { // if the filter is active
      $this->crud->addClause('where', 'status', $value);
    });
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(CardRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    $this->crud->setFromDb();

    /* FIELDS */

    /* STATUS FIELD */
    $this->crud->modifyField(
      "status",
      [ // Enum
        'name'  => 'status',
        'label' => 'Status',
        'type'  => 'enum',
        'attributes' => [
          'required' => true,
        ]
      ]
    );

    /* QUANTITY FIELD */
    $this->crud->addField(
      [   // Number
        'name' => 'quantity',
        'label' => 'Quantity',
        'type' => 'number',
        'attributes' => [
          'required' => true,
        ]
      ]
    );

    /* COLLECTION FIELD
          Modifies the field to a selector type
        */
    $this->crud->modifyField(
      "collection_id",
      [
        'name' => 'collection',
        'type' => "relationship",
        'ajax' => true,
        'inline_create' => true,
        'label' => "Collection",
        'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
        'entity' => 'collection', // the method that defines the relationship in your Model
        'model' => "App\Models\Collection", // foreign key Eloquent model
        'attributes' => [
          'required' => true,
        ]
      ]
    );

    /* IMAGE FIELD
          Modifies the field so you can upload images
        */
    $this->crud->modifyField('image', [
      'label' => "Profile Image",
      'name' => "image",
      'type' => 'image',
      'crop' => true, // set to true to allow cropping, false to disable
      'aspect_ratio' => 1, // omit or set to 0 to allow any aspect ratio
    ]);

    /* CHECKBOX FIELD
          Adds a field that checks the user agrees with the terms and conditions
        */
    $this->crud->addField(
      [
        'name'  => 'active',
        'label' => 'I accept the terms and conditions to sell my card',
        'type'  => 'checkbox',
        'attributes' => [
          'required' => true,
        ]
      ]
    );
  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }

  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }

  public function store()
    {
      // do something before validation, before save, before everything
      $response = $this->traitStore();

      $details = 'Card created succesfully';

      Mail::to('jose.carreras@vanadis.es')->send(new TestMail($details));
      // do something after save
      return $response;
    }
}
