<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Token;
use App\Helpers\Passwords;
use Illuminate\Support\Str;
use App\Helpers\PasswordGenerator;
use App\Mail\RecoverPassword;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if ($user == NULL)
            {
                return response()->json([
                    'message' => 'Wrong user or password',
                ], 401);
            }

            if (Hash::check($request->password, $user->password))
            {
                $data_token = ['email' => $user->email];
                $token_builder = new Token($data_token);
                $token = $token_builder->encode();

                return response()->json([
                    'token' => $token,
                    'email' => 'joe@doe.com',
                    'name' => 'joe'
                ], 200);
            }
            else
            {
                return response()->json([
                    'message' => 'Wrong user or password',
                ], 401);
            }
        } catch (\Throwable $th) {
            var_dump($th->getMessage()); exit;
        }
    }

    public function recover_password(Request $request)
    {
        if ($request->email == '')
        {
            return response()->json([
                'message' => 'Missing parameters',
            ], 400);
        }

        $user = User::where('email', $request->email)->first();

        if ($user == NULL)
        {
            return response()->json([
                'message' => 'User not found',
            ], 404);
        }

        $password_generator = new PasswordGenerator();
        $user->password = $password_generator->get_password_hashed();
        $user->save();

        Mail::to($user->email)->send(new RecoverPassword($password_generator));

        return response()->json([
            'message' => 'Password sent',
        ], 200);
    }
}
