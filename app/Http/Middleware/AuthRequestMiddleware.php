<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Token;
use App\User;

class AuthRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $token_header = $request->header('Authorization');
            $token = new Token();
            $token_decoded = $token->decode($token_header);

            $user = User::where('email', $token_decoded->email)->first();

            if ($user == NULL)
            {
                return response()->json([
                    'message' => 'Forbidden',
                ], 401);
            }

            return $next($request);
        }
        catch (\Throwable $th)
        {
            if ($th->getMessage() == 'Wrong number of segments')
            {
                return response()->json([
                    'message' => 'Forbidden',
                ], 401);
            }
        }
    }
}
