<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card_worker extends Model
{
    protected $fillable = ['user_id', 'card_id'];
}
