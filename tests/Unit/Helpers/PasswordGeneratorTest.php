<?php

namespace Tests\Unit\Helpers;

use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use App\Helpers\PasswordGenerator;
use App\PasswordReset;

class PasswordGeneratorTest extends TestCase
{
    public function test_password_generator_returns_text_plain_password()
    {
        $password_generator = new PasswordGenerator();

        $password_text_plain = $password_generator->get_password_text_plain();
        $first_character_password_text_plain = substr($password_text_plain, 1, 1);

        $this->assertNotEquals($first_character_password_text_plain, '$');
    }

    public function test_password_generator_returns_password_hashed()
    {
        $password_generator = new PasswordGenerator();

        $password_hashed = $password_generator->get_password_hashed();
        $first_character_password_hashed = substr($password_hashed, 1, 1);

        $this->assertEquals($first_character_password_hashed, '2');
    }

    // public function test_when_user_puts_email_its_generate_new_password()
    // {
    //     $email = 'joe@doe.com';

    //     $password_resetter = new PasswordResseter($email);

    //     $this->assertEquals(is_string($password_resetter->get_password_generated()), true);
    // }

    // public function test_password_generated_is_random()
    // {
    //     $email = 'joe@doe.com';
    //     $passwords = [];

    //     for ($i = 0; $i < 5; $i++)
    //     {
    //         $password_resetter = new PasswordResseter($email);
    //         $passwords[$i] = $password_resetter->get_password_generated();
    //     }
    //     $passwords_unique = array_unique($passwords);

    //     $this->assertEquals(count($passwords_unique), count($passwords));
    // }

    // public function test_pasword_resseter_change_the_password()
    // {
    //     $user = new User();
    //     $user->email = 'joe@doe.com';
    //     $user->password = '12345678';

    //     $password_resetter = new PasswordResseter($email);
    //     $password_resetter->get_password_generated();

    //     $this->assertEquals($password_hashed, $user->password);
    // }
}
