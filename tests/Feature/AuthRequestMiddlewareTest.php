<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Middleware\AuthRequestMiddleware;



class AuthRequestMiddlewareTest extends TestCase
{
    use RefreshDatabase;

    public function test_request_auth_that_has_correct_token_return_response_200()
    {
        $user = new User();
        $user->email = 'joe@doe.com';
        $user->password = Hash::make('12345678');
        $user->name = 'joe';
        $user->save();

        $correct_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImpvZUBkb2UuY29tIn0.yYqpHxWB9Oip-DAK4Knsub24YGOgYqrpMF7RDfDOi1o";

        $response = $this->withHeaders([
            'Authorization' => $correct_token,
        ])->json('GET', '/api/users', []);

        $response->assertStatus(200);
    }

    public function test_request_auth_that_has_token_with_not_user_in_db_return_response_401()
    {
        $user_not_in_db_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImpvZUBkb2UuY29tIn0.yYqpHxWB9Oip-DAK4Knsub24YGOgYqrpMF7RDfDOi1o";

        $response = $this->withHeaders([
            'Authorization' => $user_not_in_db_token,
        ])->json('GET', '/api/users', []);

        $response->assertStatus(401);
    }

    public function test_request_auth_that_not_has_correct_token_return_response_401()
    {
        $incorrect_token = "lkjsdljf";

        $response = $this->withHeaders([
            'Authorization' => $incorrect_token,
        ])->json('GET', '/api/users', []);

        $response->assertStatus(401);
    }

    public function test_request_auth_that_not_has_header_authorization_return_response_401()
    {
        $response = $this->json('GET', '/api/users', []);

        $response->assertStatus(401);
    }
}
