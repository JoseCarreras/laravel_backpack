<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Helpers\PasswordGenerator;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_set_user_and_password_correct_return_json_with_token()
    {
        $user = new User();
        $user->email = 'joe@doe.com';
        $user->password = Hash::make('12345678');
        $user->name = 'joe';
        $user->save();

        $response = $this->json('POST', '/api/login', [
            'email' => 'joe@doe.com',
            'password' => '12345678',
        ]);

        $user = User::where('email', 'joe@doe.com')->first();
        $data = ['email' => $user->email];
        $token_correct = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImpvZUBkb2UuY29tIn0.yYqpHxWB9Oip-DAK4Knsub24YGOgYqrpMF7RDfDOi1o";

        $response
            ->assertStatus(200)
            ->assertJson([
                'token' => $token_correct,
                'email' => 'joe@doe.com',
                'name' => 'joe'
            ]);
    }

    public function test_set_user_that_not_exist_return_wrong_password()
    {
        $response = $this->json('POST', '/api/login', [
            'email' => 'joe@doe.com',
            'password' => '12345678',
        ]);

        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Wrong user or password',
            ]);
    }

    public function test_set_user_that_exist_with_wrong_password()
    {
        $user = new User();
        $user->email = 'joe@doe.com';
        $user->password = Hash::make('12345678');
        $user->name = 'joe';
        $user->save();

        $response = $this->json('POST', '/api/login', [
            'email' => 'joe@doe.com',
            'password' => '1234',
        ]);

        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Wrong user or password',
            ]);
    }

    public function test_when_user_enters_email_if_user_not_exist_return_not_found()
    {
        $response = $this->json('POST', '/api/recover/password', [
            'email' => 'joe@doe.com',
        ]);

        $response
            ->assertStatus(404)
            ->assertJson([
                'message' => 'User not found',
            ]);
    }

    public function test_when_user_not_enters_email_return_incorrect_parameters()
    {
        $response = $this->json('POST', '/api/recover/password', [
            'email' => '',
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'message' => 'Missing parameters',
            ]);
    }

    public function test_when_user_enters_email_if_user_exist_return_password_sent()
    {
        $password_generator = new PasswordGenerator();
        $user = new User();
        $user->email = 'joe@doe.com';
        $user->password = $password_generator->get_password_hashed();
        $user->name = 'joe';
        $user->save();

        $response = $this->json('POST', '/api/recover/password', [
            'email' => 'joe@doe.com',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Password sent',
            ]);
    }

    public function test_when_user_enters_email_if_user_exist_change_his_password()
    {
        $password_generator = new PasswordGenerator();
        $user = new User();
        $user->email = 'julio.perezdecastro@vanadis.es';
        $user->password = $password_generator->get_password_hashed();
        $user->name = 'joe';
        $user->save();
        $password_text_plain_user = $password_generator->get_password_text_plain();
        $user = User::where('email', 'julio.perezdecastro@vanadis.es')->first();

        $response = $this->json('POST', '/api/recover/password', [
            'email' => 'julio.perezdecastro@vanadis.es',
        ]);
        $user_changed = User::where('email', 'julio.perezdecastro@vanadis.es')->first();
        $check_hash = Hash::check($password_text_plain_user, $user_changed->password);

        $this->assertFalse($check_hash);
    }


}
