<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Helpers\Token;

class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_the_user_create_an_acount_and_not_exist_this_user_in_DB()
    {
        $email = $this->faker->email();
        $name = $this->faker->firstName();

        $response = $this->json('POST', '/api/register', [
            'email' => $email,
            'name' => $name,
            'password' => '12345678',
            'repeat_password' => '12345678'
        ]);

        $this->assertDatabaseHas('users', [
            'email' => $email,
            'name' => $name
        ]);
    }

    public function test_the_user_create_an_acount_and_exist_this_user_in_DB()
    {
        $user = new User();
        $user->email = 'joe@doe.com';
        $user->password = Hash::make('12345678');
        $user->name = 'joe';
        $user->save();

        $response = $this->json('POST', '/api/register', [
            'email' => 'joe@doe.com',
            'name' => 'joe',
            'password' => '12345678',
            'repeat_password' => '12345678'
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'message' => 'user exist',
            ]);
    }

    public function test_the_user_password_is_hashed()
    {
        $response = $this->json('POST', '/api/register', [
            'email' => 'joe@doe.com',
            'name' => 'joe',
            'password' => '12345678',
            'repeat_password' => '12345678'
        ]);

        $user = User::where('email', 'joe@doe.com')->first();

        $this->assertFalse($user->password == '12345678');
    }

    public function test_register_generate_jwt_correct_token()
    {
        $response = $this->json('POST', '/api/register', [
            'email' => 'joe@doe.com',
            'name' => 'joe',
            'password' => '12345678',
            'repeat_password' => '12345678'
        ]);

        $user = User::where('email', 'joe@doe.com')->first();
        $data = ['email' => $user->email];
        $token_correct = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImpvZUBkb2UuY29tIn0.yYqpHxWB9Oip-DAK4Knsub24YGOgYqrpMF7RDfDOi1o";

        $response
            ->assertStatus(200)
            ->assertJson([
                'token' => $token_correct,
                'email' => 'joe@doe.com',
                'name' => 'joe'
            ]);
    }
}
